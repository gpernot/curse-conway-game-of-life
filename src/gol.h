#ifndef GOL_H
#define GOL_H

#include <stdint.h>
#include "render.h"

typedef uint64_t BASE_TYPE;

typedef struct game *game_t;

extern void game_reset(game_t g);
extern BASE_TYPE game_get(game_t g, int x, int y);
extern void game_set(game_t g, int x, int y, char s);
extern game_t game_init(int width, int height, float mutation, char *init_data);
extern void game_step(game_t g, render_context_t ctx);
extern void game_render(game_t g);
extern void game_mutation(game_t g);
extern int game_get_width(game_t g);
extern int game_get_height(game_t g);
extern void game_set_width(game_t g, int w);
extern void game_set_height(game_t g, int h);
extern int game_age(game_t g);
extern void game_performance_record(game_t g, float elapsed);
extern float game_performance(game_t g);
extern float game_performance_average(game_t g);
extern void game_pause_toggle(game_t g);
extern int game_paused(game_t g);
extern uint_fast8_t neigh_get_count(game_t g, int x, int y);

#endif
