#include <stdlib.h>
#include <ncurses.h>
#include "gol.h"
#include "curses.h"

struct curses_context {
  WINDOW *window;
  int maxx, maxy;
  char alive;
  char dead;

};

curses_context_t curses_init() {
  curses_context_t ctx = malloc(sizeof(struct curses_context));
  ctx->window = initscr();
  noecho();
  cbreak();
  clear();
  refresh();
  curs_set(0);
  getmaxyx(ctx->window ,ctx->maxx, ctx->maxy);
  ctx->alive = 'o';
  ctx->dead = ' ';
  if (has_colors()){
    start_color();
    init_color(0 , 0, 0, 0);
    init_color(8, 999, 999, 999);
    init_pair(1, 8, 0);
    attron(COLOR_PAIR(1));
  }

  return ctx;
}

void curses_close(curses_context_t ctx) {
  endwin();
  free(ctx);
}

void curses_render(curses_context_t ctx, game_t g) {
  int width = game_get_width(g);
  int height = game_get_height(g);
  char line[width + 1];
  line[width] = '\0';

  for (int y = 0; y < height; y++){
      move(y,0);
      for (int x = 0; x < width; x++){
	line[x] = game_get(g, x, y) ? ctx->alive : ctx->dead;
       printw("%s",line );
      }
  }
  refresh();

}

int curses_get_width(curses_context_t ctx) { return ctx->maxx; }
int curses_get_height(curses_context_t ctx)  { return ctx->maxy; };
