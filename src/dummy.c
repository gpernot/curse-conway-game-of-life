#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "dummy.h"
#include "gol.h"

struct dummy_context {
  int width;
  int height;
};

void dummy_init(render_context_t rctx, game_t g, bool fullscreen) {
  dummy_context_t ctx = malloc(sizeof(struct dummy_context));
  rctx->dummy = ctx;

  ctx->width = game_get_width(g);
  ctx->height = game_get_height(g);
}

void dummy_close(render_context_t rctx) {
  dummy_context_t ctx = rctx->dummy;
  free(ctx);
}

void dummy_activate_cell(render_context_t rctx, int tid, int x, int y) {
}

void dummy_setup_next_frame(render_context_t rctx) {
}

void dummy_handle_events(render_context_t rctx, game_t g) {
}

void dummy_render(render_context_t rctx, game_t g) {
}

int dummy_get_width(render_context_t rctx) { return rctx->dummy->width; }
int dummy_get_height(render_context_t rctx) { return rctx->dummy->height; }



struct render_driver dummy_driver =
  {
   "dummy", // name
   &dummy_init, // init
   &dummy_close, // close
   &dummy_setup_next_frame, // setup_next_frame
   &dummy_render, // render
   &dummy_get_width, // get_width
   &dummy_get_height, // get_height
   &dummy_activate_cell, // activate_cell
   &dummy_handle_events // handle_events
  };

