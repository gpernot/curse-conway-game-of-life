#include <SDL2/SDL.h>
#include <stdbool.h>
#include "sdl.h"
#include "gol.h"
#ifdef HAS_OPENMP
#include <omp.h>
#endif

struct sdl_context {
  SDL_Renderer *renderer;
  SDL_Window *window;
  float scale;
  int width;
  int height;
  int border;

  /* Game grid visible rectangle */
  struct {
    float xmin, ymin;
    float xmax, ymax;
  } viewport;

  // prepared points or rectangles, for each openmp thread
  struct cell {
    int index;
    union {
      SDL_Point *points;
      SDL_Rect *rects;
    };
  } *cells;

  // mouse drag motion
  int dragging;

  int max_threads;
  int max_rects_per_thread;
};

const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

const int MAX_RECTS = 1e7;

static int max(int a, int b) {
  return a>b ? a : b;
}

static float min(float a, float b) {
  return a<b ? a : b;
}

static void _reset_viewport(sdl_context_t ctx, game_t g) {
  ctx->viewport.xmin = 0;
  ctx->viewport.ymin = 0;
  ctx->viewport.xmax = game_get_width(g);
  ctx->viewport.ymax = game_get_height(g);
}

static void _update_scale(sdl_context_t ctx) {
  float w = ctx->viewport.xmax - ctx->viewport.xmin;
  float h = ctx->viewport.ymax - ctx->viewport.ymin;
  ctx->scale = min(ctx->width / w, ctx->height / h);
  ctx->border = ctx->scale >= 4 ? 1 : 0;
  //ctx->border = 0;

  if (ctx->scale >= 2) {
    // Initialize rendering with SDL_Rects
    for (int i = 0; i < ctx->max_threads; i++) {
      for (int j = 0; j < ctx->max_rects_per_thread; j++) {
	ctx->cells[i].rects[j].w = ctx->scale - ctx->border;
	ctx->cells[i].rects[j].h = ctx->scale - ctx->border;
      }
    }
  }
}

void sdl_init(render_context_t rctx, game_t g, bool fullscreen) {
  int renderer_flags, window_flags;

  sdl_context_t ctx = malloc(sizeof(struct sdl_context));
  rctx->sdl = ctx;

  ctx->dragging = 0;

  // Initialize SDL display
  renderer_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
  window_flags = fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0;

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
      printf("Couldn't initialize SDL: %s\n", SDL_GetError());
      exit(1);
    }

  ctx->window = SDL_CreateWindow("Conway's Game Of Life",
				 SDL_WINDOWPOS_UNDEFINED,
				 SDL_WINDOWPOS_UNDEFINED,
				 game_get_width(g), game_get_height(g),
				 window_flags);

  if (!ctx->window) {
      printf("Failed to open SDL window: %s\n", SDL_GetError());
      exit(1);
    }

  ctx->renderer = SDL_CreateRenderer(ctx->window, -1, renderer_flags);

  if (!ctx->renderer) {
      printf("Failed to create renderer: %s\n", SDL_GetError());
      exit(1);
    }

  // Setup context
  SDL_GetRendererOutputSize(ctx->renderer, &ctx->width, &ctx->height);

  _reset_viewport(ctx, g);
  if (ctx->viewport.xmax == 0 || ctx->viewport.ymax == 0) {
    // TODO: rework game_init so that width and height can be dynamic
    game_set_width(g, ctx->width);
    game_set_height(g, ctx->height);
  }

#ifdef HAS_OPENMP
  ctx->max_threads = omp_get_max_threads();
#else
  ctx->max_threads = 1;
#endif

  // Allocate enough SDL_Rect for all the renderings
  ctx->cells = calloc(sizeof(struct cell), ctx->max_threads);
  ctx->max_rects_per_thread = MAX_RECTS / ctx->max_threads;
  for (int i = 0; i < ctx->max_threads; i++) {
    ctx->cells[i].points = calloc(max(sizeof(SDL_Point), sizeof(SDL_Rect)), ctx->max_rects_per_thread);
    ctx->cells[i].index = 0;
  }

  _update_scale(ctx);

}


void sdl_close(render_context_t rctx) {
  sdl_context_t ctx = rctx->sdl;

  SDL_DestroyRenderer(ctx->renderer);
  SDL_DestroyWindow(ctx->window);
  SDL_Quit();
  for (int i = 0; i < ctx->max_threads; i++)
    free(ctx->cells[i].points);
  free(ctx->cells);
  free(ctx);
}

static void _render_cells(sdl_context_t ctx, int tid) {
  if (ctx->scale >= 2) {
    SDL_RenderFillRects(ctx->renderer, ctx->cells[tid].rects, ctx->cells[tid].index);
    //printf("Rendered %d rects, scale %f\n", ctx->cells[tid].index, ctx->scale);
  } else {
    SDL_RenderDrawPoints(ctx->renderer, ctx->cells[tid].points, ctx->cells[tid].index);
    //printf("Rendered %d points, scale %f\n", ctx->cells[tid].index, ctx->scale);
  }

  ctx->cells[tid].index = 0;
}

/*
  Activate a living cell if in viewport.
 */
void sdl_activate_cell(render_context_t rctx, int tid, int x, int y) {
  sdl_context_t ctx = rctx->sdl;

  if (x >= ctx->viewport.xmin && x < ctx->viewport.xmax &&
      y >= ctx->viewport.ymin && y < ctx->viewport.ymax) {
    int idx = ctx->cells[tid].index;
    if (ctx->scale >= 2) {
      ctx->cells[tid].rects[idx].x = (x - ctx->viewport.xmin) * ctx->scale + ctx->border;
      ctx->cells[tid].rects[idx].y = (y - ctx->viewport.ymin) * ctx->scale + ctx->border;
      ctx->cells[tid].index++;
    } else if (ctx->scale >= 1) {
      ctx->cells[tid].points[idx].x = (x - ctx->viewport.xmin) * ctx->scale + ctx->border;
      ctx->cells[tid].points[idx].y = (y - ctx->viewport.ymin) * ctx->scale + ctx->border;
      ctx->cells[tid].index++;
    } else {
      /* at small scales, only draw sub-pixel at the center of a pixel */
      int r = (1 / ctx->scale);
      if (x % r < (r/2+1) && x % r >= (r/2-1) &&
	  y % r < (r/2+1) && y % r >= (r/2-1)) {
	ctx->cells[tid].points[idx].x = (x - ctx->viewport.xmin) * ctx->scale + ctx->border;
	ctx->cells[tid].points[idx].y = (y - ctx->viewport.ymin) * ctx->scale + ctx->border;
	ctx->cells[tid].index++;
      }
    }

    if (ctx->cells[tid].index >= ctx->max_rects_per_thread) {
      // flush points buffer if overflow
      // TODO: race condition here
      _render_cells(ctx, tid);
    }
  }
}

static void _viewport_pan(sdl_context_t ctx, game_t g) {
  if (ctx->viewport.xmin < 0) ctx->viewport.xmin = 0;
  if (ctx->viewport.ymin < 0) ctx->viewport.ymin = 0;
  if (ctx->viewport.xmax >= game_get_width(g))
    ctx->viewport.xmax = game_get_width(g) - 1;
  if (ctx->viewport.ymax >= game_get_height(g))
    ctx->viewport.ymax = game_get_height(g) - 1;
}

static void _zoom(sdl_context_t ctx, game_t g, int factor) {
  int center_x, center_y;

  SDL_GetMouseState(&center_x, &center_y);

  int xmin = ctx->viewport.xmin;
  int ymin = ctx->viewport.ymin;
  int xmax = ctx->viewport.xmax;
  int ymax = ctx->viewport.ymax;

  center_x = ctx->viewport.xmin + center_x / ctx->scale;
  center_y = ctx->viewport.ymin + center_y / ctx->scale;

  if (center_x >= game_get_width(g)) center_x = game_get_width(g) - 1;
  if (center_y >= game_get_height(g)) center_y = game_get_height(g) - 1;

  ctx->viewport.xmin += (center_x - ctx->viewport.xmin) / (10 * factor);
  ctx->viewport.ymin += (center_y - ctx->viewport.ymin) / (10 * factor);
  ctx->viewport.xmax += (center_x - ctx->viewport.xmax) / (10 * factor);
  ctx->viewport.ymax += (center_y - ctx->viewport.ymax) / (10 * factor);

  if (ctx->viewport.xmin >= ctx->viewport.xmax) {
    ctx->viewport.xmin = xmin;
    ctx->viewport.xmax = xmax;
  }
  if (ctx->viewport.ymin >= ctx->viewport.ymax) {
    ctx->viewport.ymin = ymin;
    ctx->viewport.ymax = ymax;
  }

  _viewport_pan(ctx, g);

#ifndef NDEBUG
  printf("viewport %f,%f / %f,%f\n", ctx->viewport.xmin, ctx->viewport.ymin,
   	 ctx->viewport.xmax, ctx->viewport.ymax);
#endif

  _update_scale(ctx);

}

static void _drag(sdl_context_t ctx, game_t g, int xrel, int yrel) {

  float move_x = -xrel / ctx->scale;
  float move_y = -yrel / ctx->scale;

  if (ctx->viewport.xmin + move_x < 0)
    move_x = - ctx->viewport.xmin;
  if (ctx->viewport.xmax + move_x > game_get_width(g))
    move_x = game_get_width(g) - ctx->viewport.xmax;
  ctx->viewport.xmin += move_x;
  ctx->viewport.xmax += move_x;

  if (ctx->viewport.ymin + move_y < 0)
    move_y = - ctx->viewport.ymin;
  if (ctx->viewport.ymax + move_y > game_get_height(g))
    move_y = game_get_height(g) - ctx->viewport.ymax;
  ctx->viewport.ymin += move_y;
  ctx->viewport.ymax += move_y;

  _viewport_pan(ctx, g);
}

/*
 Prepare next frame
 */
void sdl_setup_next_frame(render_context_t rctx) {
  sdl_context_t ctx = rctx->sdl;


  /* reset points buffers */
  for (int tid = 0; tid < ctx->max_threads; tid++) {
    ctx->cells[tid].index = 0;
  }

  /* clean-up surface */
  SDL_SetRenderDrawColor(ctx->renderer, 0, 0, 0, 0);
  SDL_RenderClear(ctx->renderer);
  SDL_SetRenderDrawColor(ctx->renderer, 128, 128, 128, 0);
}

void sdl_handle_events(render_context_t rctx, game_t g) {
  sdl_context_t ctx = rctx->sdl;

  // Handle events
  SDL_Event event;
  while (SDL_PollEvent(&event))
    {
      switch (event.type)
	{
	case SDL_QUIT:
	  printf("%d @ %f fps - Avg: %f fps\n",
		 game_age(g), 1000/game_performance(g), 1000/game_performance_average(g));

	  exit(0);
	  break;
	case SDL_KEYDOWN:
	  switch (event.key.keysym.sym) {
	  case SDLK_ESCAPE:
	    printf("%d @ %f fps - Avg: %f fps\n",
		   game_age(g), 1000/game_performance(g), 1000/game_performance_average(g));
	    exit(0);
	    break;
	  case SDLK_1:
	    _reset_viewport(ctx, g);
	    _update_scale(ctx);
	    break;
	  case SDLK_SPACE:
	    game_pause_toggle(g);
	    break;
	  }
	  break;

	case SDL_MOUSEWHEEL:
	  _zoom(ctx, g, event.wheel.y);
	  break;

	case SDL_MOUSEBUTTONDOWN:
	  if (event.button.button == SDL_BUTTON_LEFT) {
	    //SDL_SetRelativeMouseMode(SDL_TRUE);
	    int xrel, yrel;
	    SDL_GetRelativeMouseState(&xrel, &yrel);
	    ctx->dragging = 1;
	  }
	  break;
	case SDL_MOUSEBUTTONUP:
	  if (event.button.button == SDL_BUTTON_LEFT) {
	    //SDL_SetRelativeMouseMode(SDL_FALSE);
#ifndef NDEBUG
	    int i = ctx->viewport.xmin + event.button.x / ctx->scale;
	    int j = ctx->viewport.ymin + event.button.y / ctx->scale;
	    printf("clicked at %d,%d - neigh_count = %d\n",
		   i, j,
		   neigh_get_count(g, i, j));
#endif
	    ctx->dragging = 0;
	  }
	  break;
	case SDL_MOUSEMOTION:
	  if (ctx->dragging) {
	    int xrel, yrel;
	    SDL_GetRelativeMouseState(&xrel, &yrel);
	    _drag(ctx, g, xrel, yrel);
	  }
	  break;
	default:
	  break;
	}
    }
}

void sdl_render(render_context_t rctx, game_t g) {
  sdl_context_t ctx = rctx->sdl;

  if (game_paused(g)) {
    // game is paused : render according to viewport
    for (int y = ctx->viewport.ymin; y < ctx->viewport.ymax; y++) {
      for (int x = ctx->viewport.xmin; x < ctx->viewport.xmax; x++) {
	if (game_get(g, x, y)) {
	  sdl_activate_cell(rctx, 0, x, y);
	}
      }
    }
  }

  // Render remaining undrawn cells
  for (int tid = 0; tid < ctx->max_threads; tid++) {
    _render_cells(ctx, tid);
  }

  SDL_RenderPresent(ctx->renderer);

}

int sdl_get_width(render_context_t rctx) { return rctx->sdl->width; }
int sdl_get_height(render_context_t rctx) { return rctx->sdl->height; }

struct render_driver sdl_driver =
  {
   "sdl", // name
   &sdl_init, // init
   &sdl_close, // close
   &sdl_setup_next_frame, // setup_next_frame
   &sdl_render, // render
   &sdl_get_width, // get_width
   &sdl_get_height, // get_height
   &sdl_activate_cell, // activate_cell
   &sdl_handle_events // handle_events
  };

