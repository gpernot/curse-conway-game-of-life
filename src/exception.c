#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

bool exception = false;

static int _code;
static char *_message = NULL;

void raise_exception(int code, char *message, ...){
  va_list args;
  va_start(args, message);

  exception = true;
  _code = code;
  if (_message != NULL)
    free(_message);
  vasprintf(&_message, message, args);
  va_end(args);
}

void get_exception(int *code, char **message) {
  *code = _code;
  *message = _message;
  exception = false;
}

void clear_exception() {
  exception = false;
  if (_message != NULL)
    free(_message);
}
