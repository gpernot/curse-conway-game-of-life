#include <stdio.h>
#include <stdint.h>

/*
unsigned int v_increment_table[16] = {
					   0x00000000, // 0000
					   0x00000101, // 0001
					   0x00010101, // 0010
					   0x00010202, // 0011
					   0x01010100, // 0100
					   0x01010201, // 0101
					   0x01020201, // 0110
					   0x01020302, // 0111
					   0x01010000, // 1000
					   0x01010101, // 1001
					   0x01020101, // 1010
					   0x01020202, // 1011
					   0x02020100, // 1100
					   0x02020201, // 1101
					   0x02030201, // 1110
					   0x02030302  // 1111
};
unsigned int h_increment_table[16] = {
					   0x00000000, // 0000
					   0x00000100, // 0001
					   0x00010001, // 0010
					   0x00010101, // 0011
					   0x01000100, // 0100
					   0x01000200, // 0101
					   0x01010101, // 0110
					   0x01010201, // 0111
					   0x00010000, // 1000
					   0x00010100, // 1001
					   0x00020001, // 1010
					   0x00020101, // 1011
					   0x01010100, // 1100
					   0x01010200, // 1101
					   0x01020101, // 1110
					   0x01020201  // 1111
};
*/


// TODO : build these 2 tables
uint64_t v_increment32[0x100];
uint64_t h_increment32[0x100];

uint64_t v_increment64[0x10000];
uint64_t h_increment64[0x10000];

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"

#define BYTE_TO_BINARY(byte)			\
  (byte & 0x80 ? '1' : '0'),			\
    (byte & 0x40 ? '1' : '0'),			\
    (byte & 0x20 ? '1' : '0'),			\
    (byte & 0x10 ? '1' : '0'),			\
    (byte & 0x08 ? '1' : '0'),			\
    (byte & 0x04 ? '1' : '0'),			\
    (byte & 0x02 ? '1' : '0'),			\
    (byte & 0x01 ? '1' : '0')

static uint64_t _big_endian_increment(uint64_t i) {
#if BIG_ENDIAN_ARCH == 0
  return
    ((i & 0xff00000000000000ULL) >> (7*8)) |
    ((i & 0x00ff000000000000ULL) >> (5*8)) |
    ((i & 0x0000ff0000000000ULL) >> (3*8)) |
    ((i & 0x000000ff00000000ULL) >> (1*8)) |
    ((i & 0x00000000ff000000ULL) << (1*8)) |
    ((i & 0x0000000000ff0000ULL) << (3*8)) |
    ((i & 0x000000000000ff00ULL) << (5*8)) |
    ((i & 0x00000000000000ffULL) << (7*8));
#else
  return i;
#endif
}

void increments_init() {
  for (uint32_t i = 0; i < 0x10000; i++) {
    uint64_t v_incr = 0;
    uint64_t h_incr = 0;

    /* LSB */
    v_incr |= __builtin_popcountll(i & 0b11);
    h_incr |= __builtin_popcountll(i & 0b10);

    /* MSB */
    v_incr |= (uint64_t)__builtin_popcountll(i & (0b11 << 14)) << ((sizeof(uint64_t) - 1)*8 + 4);
    h_incr |= (uint64_t)__builtin_popcountll(i & (0b01 << 14)) << ((sizeof(uint64_t) - 1)*8 + 4);

    for (int byte = 1; byte < 15; byte ++) {
      uint64_t vmask = 0b111 << (byte-1);
      uint64_t hmask = 0b101 << (byte-1);

      v_incr |= (uint64_t)__builtin_popcountll(i & vmask) << (byte * 4);
      h_incr |= (uint64_t)__builtin_popcountll(i & hmask) << (byte * 4);
    }
    v_increment64[i] = _big_endian_increment(v_incr);
    h_increment64[i] = _big_endian_increment(h_incr);

#ifndef NDEBUG
    if (i &0x0180)
    printf("%016lx\n" BYTE_TO_BINARY_PATTERN BYTE_TO_BINARY_PATTERN "\n\n",
	   _big_endian_increment(v_increment64[i]), BYTE_TO_BINARY(i>>8), BYTE_TO_BINARY(i&0xff));
#endif
  }
}
