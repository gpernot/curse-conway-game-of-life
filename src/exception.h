
extern int exception;

extern void raise_exception(int code, char *message, ...);
extern void get_exception(int *code, char **message);
