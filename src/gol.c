#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include "gol.h"
#include "render.h"
#include "increments.h"

#ifdef HAS_OPENMP
#include <omp.h>
#endif

#define RECORD_SIZE (8 * sizeof(BASE_TYPE))
const BASE_TYPE RECORD_MASK = RECORD_SIZE - 1;


struct game {
  BASE_TYPE *grid;
  int grid_size;
  int width;
  int height;
  union {
    uint8_t * as_char;
    uint64_t * as_uint64;
  } neighbour_counts ;
  float mutation;

  // timing fields
  int age;
  float cumulative_elapsed;
  float rendering_time;

  int paused;
};


/*
 * Neighbour counts access routines
 *
 * Counts are stored in 4 bits, in a shared byte.
 * Higher bits for left cell, lower bits for the right cell.
 */

/* neighbour_counts allocator */
static void neigh_init(game_t g) {
  g->neighbour_counts.as_char = calloc(g->width * g->height / 2, sizeof(unsigned char));
}

static void neigh_reset(game_t g) {
  memset(g->neighbour_counts.as_char, 0,
	 g->width * g->height / 2 * sizeof(unsigned char));
}

/* Return offset in neighbour_counts.as_char for coordinates(x,y). */
static uint64_t neigh_char_offset(game_t g, int x, int y) {
  return (x + y * g->width) / 2;
}

/* Return offset in neighbour_counts.as_uint64 for coordinates(x,y). */
static uint64_t neigh_uint64_offset(game_t g, int x, int y) {
  return (x + y * g->width) / sizeof(uint64_t) / 2;
}

/* ++ (char) operator */
static void neigh_inc_char(game_t g, int x, int y) {
  if (x & 1) {
    /* lower nibble */
    g->neighbour_counts.as_char[neigh_char_offset(g, x, y)] ++;
  } else {
    /* higher nibble */
    g->neighbour_counts.as_char[neigh_char_offset(g, x, y)] += 0x10;
  }
}

/* += (uint64) operator */
static void neigh_iadd_uint64(game_t g, int x, int y, uint64_t value) {
  g->neighbour_counts.as_uint64[neigh_uint64_offset(g, x, y)] += value;
}

/* "get count" method */
uint_fast8_t neigh_get_count(game_t g, int x, int y) {
  if (x & 1) {
    /* lower nibble */
    return g->neighbour_counts.as_char[neigh_char_offset(g, x, y)] & 0xf;
  } else {
    /* higher nibble */
    return g->neighbour_counts.as_char[neigh_char_offset(g, x, y)] >> 4;
  }
}


/*
 * Game grid routines
 */

int game_get_width(game_t g) { return g->width; }
int game_get_height(game_t g)  { return g->height; }
void game_set_width(game_t g, int w) { g->width = w; };
void game_set_height(game_t g, int h) { g->height = h;};

void game_reset(game_t g){
  time_t t;
  srandom((int)time(&t));
  for (int i = 0; i < g->grid_size;i++){
    if (sizeof(BASE_TYPE) <= sizeof(int))
      g->grid[i] = random() | (random() & 1) << (RECORD_SIZE - 1);
    else
      //g->grid[i] = ~(BASE_TYPE)0U;
      g->grid[i] = ((BASE_TYPE)random() << RECORD_SIZE/2) + random();
  }
}

/*
  Return bit-index for coordinate x
  Pixels are stored MSB-first, ie leftmost pixel is stored in Most Significant Bit
 */
static unsigned int _bit_index(int x) {
  return RECORD_SIZE - (x & RECORD_MASK) - 1;
}

/*
  Return offset in buffer for coordinate x,y
 */
static uint_fast32_t _buffer_offset(game_t g, int x, int y) {
  return (x + g->width * y) / RECORD_SIZE;
}

BASE_TYPE game_get(game_t g, int x, int y) {
  if (x < 0 || y < 0 || x >= g->width || y >= g->height) {
    return 0;
  }
  return (g->grid[_buffer_offset(g, x, y)] >> _bit_index(x)) & 1;
}

void game_set(game_t g, int x, int y, char s){
  BASE_TYPE bitmask;
  if (x < 0 || y < 0 || x >= g->width || y >= g->height) {
    return;
  }
  bitmask = 1ULL << _bit_index(x);
  if (s != 0)
    g->grid[_buffer_offset(g, x, y)] |= bitmask;
  else
    g->grid[_buffer_offset(g, x, y)] &= ~bitmask;
}

game_t game_init(int width, int height, float mutation, char* data){
  game_t g = malloc(sizeof(struct game));

  g->width = width;
  g->height = height;
  g->width &= ~RECORD_MASK;
  if (g->width < RECORD_SIZE)
    g->width = RECORD_SIZE;
  g->grid_size = g->width * g->height / RECORD_SIZE;
  g->grid = calloc(g->grid_size, sizeof(BASE_TYPE));
  g->mutation = mutation;
  g->age = 0;
  g->rendering_time = 0;
  g->cumulative_elapsed = 0;
  g->paused = 0;

  if (data != NULL) {
    for (int x = 0 ; x < width; x++)
      for (int y = 0 ; y < height; y++)
	game_set(g, x, y, data[x+y*width] == 'x' ? 1 : 0);
  } else {
    game_reset(g);
  }

  neigh_init(g);
  increments_init();
  return g;
}

static void _increment16(uint_fast16_t val, uint8_t *h_incr, uint8_t *v_incr, int first, int last) {

  if (val & 0x8000 && !first) {
    /* add carry to the lower nibble of the previous byte */
    *(v_incr-1) += 1;
    *(h_incr-1) += 1;
  }

  if (val & 0x01 && !last) {
    /* add carry to the higher nibble of the next byte */
    *(v_incr+2*sizeof(uint64_t)) += 0x10;
    *(h_incr+2*sizeof(uint64_t)) += 0x10;
  }

  *(uint64_t*)h_incr += h_increment64[val];
  *(uint64_t*)v_incr += v_increment64[val];

}

static void _increment8(uint_fast8_t val, uint8_t *h_incr, uint8_t *v_incr, int first, int last) {

  if (val & 0x80 && !first) {
    /* add carry to the lower nibble of the previous byte */
    *(v_incr-1) += 1;
    *(h_incr-1) += 1;
  }

  if (val & 0x01 && !last) {
    /* add carry to the higher nibble of the next byte */
    *(v_incr+sizeof(uint64_t)) += 0x10;
    *(h_incr+sizeof(uint64_t)) += 0x10;
  }

  *(uint64_t*)h_incr += h_increment32[val];
  *(uint64_t*)v_incr += v_increment32[val];

}

static void _apply_increments(game_t g, int x, int y, uint8_t *h_incr, uint8_t *v_incr,
			      int left_carry, int right_carry) {

  /* apply increments on previous line */
  if (y > 0) {
    for (uint_fast8_t i = 0; i < RECORD_SIZE / sizeof(uint64_t); i++)
      neigh_iadd_uint64(g, x + i * sizeof(uint64_t), y-1, ((uint64_t*)v_incr)[i]);
  }

  /* apply increments on next line */
  if (y < g->height-1) {
    for (uint_fast8_t i = 0; i < RECORD_SIZE / sizeof(uint64_t); i++)
      neigh_iadd_uint64(g, x + i * sizeof(uint64_t), y+1, ((uint64_t*)v_incr)[i]);
  }

  /* apply increments on current line */
  for (uint_fast8_t i = 0; i < RECORD_SIZE / sizeof(uint64_t); i++)
    neigh_iadd_uint64(g, x + i * sizeof(uint64_t), y, ((uint64_t*)h_incr)[i]);

  /* Apply carry in leftmost cells */
  if (x > 0 && left_carry) {
    neigh_inc_char(g, x-1, y);
    if (y > 0)
      neigh_inc_char(g, x-1, y-1);
    if (y < g->height-1)
      neigh_inc_char(g, x-1, y+1);
  }

  /* Apply carry in rightmost cells */
  if (x < g->width - RECORD_SIZE && right_carry) {
    neigh_inc_char(g, x + RECORD_SIZE, y);
    if (y > 0)
      neigh_inc_char(g, x + RECORD_SIZE, y-1);
    if (y < g->height-1)
      neigh_inc_char(g, x + RECORD_SIZE, y+1);
  }

}

/* Compute neighbour_counts table */
static void game_neighbour(game_t g){

  uint_fast16_t val;
  uint8_t h_incr[RECORD_SIZE];
  uint8_t v_incr[RECORD_SIZE];
  int left_carry, right_carry;
  int x;
  int y;
  BASE_TYPE block;

  neigh_reset(g);

#pragma omp parallel shared(g) private(val, h_incr, v_incr, left_carry, right_carry, x, y, block)
  {
#pragma omp for ordered
    for (y = 0; y < g->height; y++) {
      for (x = 0; x <= g->width - RECORD_SIZE; x += RECORD_SIZE) {

	block = g->grid[_buffer_offset(g, x, y)];

	memset(h_incr, 0, RECORD_SIZE * sizeof(char));
	memset(v_incr, 0, RECORD_SIZE * sizeof(char));

	switch (RECORD_SIZE) {
	case 64:
	  // (block & 0xffff000000000000)
	  val = (block & 0xffff000000000000ULL) >> (6*8);
	  _increment16(val, h_incr, v_incr, true, false);
	  left_carry = (val >> 15) & 1;

	  // (block & 0x0000ffff00000000)
	  val = (block & 0x0000ffff00000000ULL) >> (4*8);
	  _increment16(val, h_incr+16, v_incr+16, false, false);

	  // (block & 0x00000000ffff0000)
	  val = (block & 0x00000000ffff0000ULL) >> (2*8);
	  _increment16(val, h_incr+32, v_incr+32, false, false);

	  // (block & 0x000000000000ffff)
	  val = (block & 0x000000000000ffffULL);
	  _increment16(val, h_incr+48, v_incr+48, false, true);
	  right_carry = val & 1;
	  break;

	case 32:
	  // (block & 0xff000000)
	  val = (block & 0xffff0000) >> 16;
	  _increment16(val, h_incr, v_incr, true, false);
	  left_carry = (val >> 7) & 1;

	  // (block & 0x00ff0000)
	  val = (block & 0x0000ffff);
	  _increment16(val, h_incr+16, v_incr+16, false, true);
	  right_carry = val & 1;
	  break;

	  // TODO: re-implement this when _increment8 is fully implemented
	  /*
	case 16:
	  // (block & 0xff00)
	  val = (block & 0xff00) >> 8;
	  _increment8(val, h_incr, v_incr, true, false);
	  left_carry = (val >> 7) & 1;

	  // (block & 0xff)
	  val = (block & 0x00ff);
	  _increment8(val, h_incr+8, v_incr+8, false, true);
	  right_carry = val & 1;
	  break;

	case 8:
	  // (block & 0xff)
	  val = (block & 0xff);
	  _increment8(val, h_incr, v_incr, true, true);
	  left_carry = (val >> 7) & 1;
	  right_carry = val & 1;
	  break;
	  */
	default:
	  printf("Invalid record size.\n");
	  exit(0);
	}

	// Finaly, compute neighbour values
/* #pragma omp critical */
	_apply_increments(g, x, y, h_incr, v_incr, left_carry, right_carry);
      }
    }
  }
}

static char game_nextstate(char current, int neigh){
  if (current != 0) {
    if (neigh == 2 || neigh == 3){
      return 1;
    }else{
      return 0;
    }
  }else{
    if (neigh == 3){
      return 1;
    }else{
      return 0;
    }
  }
}

void game_step(game_t g, render_context_t rctx){
  int x,y;
  uint_fast8_t neigh;
  char current, newstate;
  int tid = 0;

  if (g->paused)
    return;

  game_neighbour(g);
#ifdef HAS_OPENMP
#pragma omp parallel shared(g, rctx) private(x,y,neigh,current,newstate,tid)
  {
    tid = omp_get_thread_num();
#pragma omp for ordered
#endif
    // loop over y first, so game_set() below wont conflict between threads
    for (y = 0; y < g->height; y++){
      for (x = 0; x < g->width; x++){
        neigh = neigh_get_count(g, x, y);
        current = game_get(g, x, y);
        newstate = game_nextstate(current, neigh);
	game_set(g, x, y, newstate);
        if (newstate) {
#ifdef NDEBUG
/* #pragma omp critical */
	  render_activate_cell(rctx, tid, x, y);
#endif
        }
      }
    }
#ifdef HAS_OPENMP
  }
#endif
}

void game_mutation(game_t g){
  uint64_t r = random();
  for (int mutation_amount = g->mutation * (float)g->width * (float)g->height; mutation_amount>0; --mutation_amount){
    r *= 2147483647;
    int x = r % g->width;
    int y = (r >> (sizeof(int))) % g->height;
    g->grid[_buffer_offset(g,x,y)] ^= 1ULL << _bit_index(x);
    //game_set(g,x,y,1-game_get(g,x,y));
  }
}

int game_age(game_t g) {
  return g->age;
}

void game_performance_record(game_t g, float elapsed) {
  g->age++;
  g->rendering_time = elapsed;
  g->cumulative_elapsed += elapsed;
}

float game_performance(game_t g) {
  if (g->rendering_time > 0)
    return g->rendering_time;
  else
    return 1;
}

float game_performance_average(game_t g) {
  if (g->age > 0)
    return g->cumulative_elapsed / g->age;
  else
    return 1;
}

void game_pause_toggle(game_t g) { g->paused = !g->paused; }
int game_paused(game_t g) { return g->paused; }

