#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include <sys/time.h>
#include <time.h>
#include <getopt.h>
#include "gol.h"
#include "render.h"
#include "exception.h"
#include "render.h"

/*
void print_speed(game_t g, float elapsed){
  move(0,0);
  printw("%f", elapsed);
  move(1,0);
  printw("%f",g->cumulative_elapsed / g->age);
}
*/

char test_data2[] = {"----    " "        " "        " "      xx""        " "        " "        " "        "
		     "  x     " "        " "        " "      xx""        " "        " "        " "        "
		     "x x     " "      x " "        " "        ""        " "        " "        " "        "
		     " xx     " "    x x " "        " "        ""        " "        " "        " "        "
		     "        " "     xx " "        " "        ""        " "        " "        " "        "
		     "        " "        " "        " "        ""        " "        " "        " "        "
		     "        " "        " "        " "      xx""        " "        " "        " "        "
		     "        " "        " "        " "      xx""        " "        " "        " "        "
};

char test_data[] = {"        " "        " "        " "      xx""        " "        " "        " "        "
		    "  x     " "        " "        " "      xx""        " "        " "        " "        "
		    "x x     " "     xxx" "        " "        ""        " "        " "        " "        "
		    " xx     " "        " "        " "        ""        " "        " "        " "        "
		    "        " "        " "        " "        ""        " "        " "        " "        "
		    "        " "        " "        " "        ""        " "        " "        " "        "
		    "        " "        " "        " "        ""        " "        " "        " "        "
		    "        " "        " "        " "        ""        " "        " "        " "        "

};

void sleep_sec(float sec_f){
  struct timespec ts;
  ts.tv_sec = (int) sec_f;
  ts.tv_nsec = (sec_f - ts.tv_sec) * 1E9;
  nanosleep(&ts, &ts);
}

static double elapsed(struct timeval *begin, struct timeval *end) {
  return fabs((end->tv_sec * 1e6 + end->tv_usec) -
	      (begin->tv_sec * 1e6 + begin->tv_usec));
}

int main(int argc, char **argv){
  time_t t;
  struct timeval begin, last_render, last_generation;

  srandom((unsigned) time(&t));
  double mutation_rate = -1;
  bool fullscreen = false;
  double fps = 0;
  double gps = 0;
  int width = 800;
  int height = 800;
  int count = 0;
  bool frame_ready = false;

  while (1){
    int option_index = 0;
    static struct option longoption[] = {
					 {"fps", required_argument, 0, 'f'},
					 {"gps", required_argument, 0, 'g'},
					 {"fullscreen", no_argument, 0, 'F'},
					 {"mutation", required_argument, 0, 'm'},
					 {"count", required_argument, 0, 'c'},
					 {"width", required_argument, 0, 'w'},
					 {"height", required_argument, 0, 'h'},
					 {0, 0, 0, 0}
    };
    char c = getopt_long(argc,argv,"f:g:Fm:w:h:c:", longoption, &option_index);
    if(c == -1){
      break;
    }
    switch(c){
    case 'm':
      mutation_rate = atof(optarg);
      break;
    case 'F':
      fullscreen = true;
      break;
    case 'f':
      fps = atof(optarg);
      break;
    case 'g':
      gps = atof(optarg);
      break;
    case 'c':
      count = atoi(optarg);
      break;
    case 'w':
      width = atoi(optarg);
      break;
    case 'h':
      height = atoi(optarg);
      break;
    }
  }

  game_t globalgame = game_init(width, height, mutation_rate, NULL);

  render_context_t renderer = render_init("dummy", globalgame, fullscreen);

  if (renderer == NULL) {
    if (exception) {
      int code;
      char *message;
      get_exception(&code, &message);
      printf("Exception #%d raised : %s\n", code, message);
      exit(code);
    } else {
      printf("An unkown error occured in render_init().\n");
      exit(1);
    }
  }

  last_render.tv_sec = 0;
  last_render.tv_usec = 0;

  while (1){

    gettimeofday(&begin, NULL);

    if ((fps == 0
	 || elapsed(&last_render, &begin)*1e-6 > 1/fps)
	&& frame_ready) {
      render_render(renderer, globalgame);
      render_setup_next_frame(renderer);

      printf("\033[2K%d @ %f fps - Avg: %f fps\r",
	     game_age(globalgame),
	     1000/game_performance(globalgame),
	     1000/game_performance_average(globalgame));
      fflush(stdout);

      gettimeofday(&last_render, NULL);
    }

    render_handle_events(renderer, globalgame);

    if ((gps == 0
	 ||elapsed(&last_generation, &begin)*1e-6 > 1/gps)
	&& !game_paused(globalgame)) {
      if (mutation_rate > 0) game_mutation(globalgame);
      game_step(globalgame, renderer);
      gettimeofday(&last_generation, NULL);
      game_performance_record(globalgame, elapsed(&begin, &last_generation)*1e-3);
      frame_ready = true;
    } else {
      frame_ready = false;
    }

    double wait_fps = 1/fps - elapsed(&last_render, &begin)*1e-6;
    double wait_gps = 1/fps - elapsed(&last_generation, &begin)*1e-6;
    double wait = wait_fps < wait_gps ? wait_fps : wait_gps;
    if (wait > 0) {
      sleep_sec( wait);
    }

    if (count != 0 && game_age(globalgame) > count) {
      printf("\n");
      break;
    }
  }
  render_close(renderer);
}
