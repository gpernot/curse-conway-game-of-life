#include <stdlib.h>
#include <string.h>
#include "exception.h"
#include "render.h"
#include "sdl.h"
#include "dummy.h"

static render_driver_t drivers[] = { &sdl_driver,
				     &dummy_driver,
				     (render_driver_t)NULL };

render_context_t render_init(const char *driver_name, game_t g, bool fullscreen) {
  render_context_t rctx = malloc(sizeof(struct render_context));
  int i;
  for (i = 0;
       drivers[i] != NULL && strcmp(drivers[i]->name, driver_name);
       i++);
  if (drivers[i] == NULL) {
    raise_exception(1, "Invalid driver name [%s].", driver_name);
    return NULL;
  }
  rctx->driver = drivers[i];
  (*rctx->driver->init)(rctx, g, fullscreen);
  return rctx;
}

void render_close(render_context_t rctx) {
  (*rctx->driver->close)(rctx);
  free(rctx);
}

void render_setup_next_frame(render_context_t rctx) {
  return (*rctx->driver->setup_next_frame)(rctx);
}

void render_render(render_context_t rctx, game_t g) {
  return (*rctx->driver->render)(rctx, g);
}

int render_get_width(render_context_t rctx) {
  return (*rctx->driver->get_width)(rctx);
}

int render_get_height(render_context_t rctx) {
  return (*rctx->driver->get_height)(rctx);
}

void render_activate_cell(render_context_t rctx, int tid, int x, int y)
{
  (*rctx->driver->activate_cell)(rctx, tid, x, y);
}

void render_handle_events(render_context_t rctx, game_t g) {
  return (*rctx->driver->handle_events)(rctx, g);
}
